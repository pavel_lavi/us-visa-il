FROM python:3.10-slim
ARG GECKO_VERSION=0.30.0

COPY requirements.txt .

RUN apt-get -y update && apt-get -y upgrade &&\
    apt-get install -y --no-install-recommends firefox-esr xvfb wget &&\
    wget https://github.com/mozilla/geckodriver/releases/download/v${GECKO_VERSION}/geckodriver-v${GECKO_VERSION}-linux64.tar.gz &&\
    tar -C /usr/local/bin/ -xvf geckodriver-v${GECKO_VERSION}-linux64.tar.gz &&\
    rm geckodriver-v${GECKO_VERSION}-linux64.tar.gz &&\
    pip3 install -r requirements.txt

WORKDIR /usvisa-app

CMD ["./main.py"]