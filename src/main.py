#!/usr/bin/env python
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from datetime import datetime
from os import path, environ
from element_paths import ElementPath
from constants import Constants
import browser_factory
import bot_null
from app_scheduler import AppScheduler
import telegram
import logging

# Logging
logging.basicConfig(level=logging.INFO,
                    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log = logging.getLogger('usvisa')

# Config
visa_username = "user@gmail.com"
visa_password = "password123"
telegram_token = "bot_token123"
telegram_chat_id = "111111"
current_appointment_year = 2022
current_appointment_month = 11
current_appointment_day = 8

if telegram_token is not None:
    bot = telegram.Bot(token=telegram_token)
else:
    bot = bot_null.BotNull(log)


def find_appointment(app_os):
    if path.exists(AppScheduler.get_app_filepath_by_context(app_os)):
        log.info("new appointment already set! Exiting...")
        exit(0)
    log.info('running in {0} context'.format(app_os))
    browser = browser_factory.BrowserFactory(log).get_browser(app_os)
    browser.get(Constants.MAIN_URL)
    scheduler = AppScheduler(log, browser, app_os, bot)
    try:
        try:
            button = browser.find_element(By.XPATH, ElementPath.LOGIN_BUTTON_XPATH)
            if button is not None:
                button.click()
        except NoSuchElementException:
            log.info('Button not found, moving on...')

        log.info('Logging in {0}'.format(visa_username))

        # login screen
        browser.find_element(By.ID, ElementPath.USER_EMAIL_ID).send_keys(visa_username)
        browser.find_element(By.ID, ElementPath.USER_PASSWORD_ID).send_keys(visa_password)
        browser.find_element(By.XPATH, ElementPath.POLICY_AGREEMENT_CHECKBOX_XPATH).click()
        browser.find_element(By.XPATH, ElementPath.SIGNIN_BUTTON_XPATH).click()

        # main page
        group = WebDriverWait(browser, Constants.LOGIN_WAIT_TIMEOUT_SEC) \
            .until(ec.presence_of_element_located((By.CSS_SELECTOR, ElementPath.ACTIVE_GROUP_CARD_CLASS)))
        group.find_element(By.CSS_SELECTOR, ElementPath.CONTINUE_BUTTON_CLASS).click()

        # actions page
        appointments_accordion = browser.find_elements(By.CLASS_NAME, ElementPath.ACCORDION_BUTTONS_CLASS)

        appointment_reschedule = [el for el in appointments_accordion if el.text == Constants.RESCHEDULE_TEXT_HEB][0]
        appointment_reschedule.click()
        appointment_button = appointment_reschedule.find_element(By.CSS_SELECTOR, ElementPath.ACTION_BUTTONS_CLASS)
        appointment_button.click()

        # find appointment
        log.info("looking for an appointment date")
        current_app_datetime = datetime(current_appointment_year, current_appointment_month, current_appointment_day)
        time.sleep(3)

        from selenium.webdriver.support.select import Select
        for facility_id in range(2, 0, -1):
            log.info(f"Checking appointments for {facility_id}, where 1==Jerusalem 2==Tel-aviv")
            appointments = []
            browser.find_element(By.ID, "appointments_consulate_address").click()

            # dummy click to close datepicker
            selector = Select(browser.find_element(By.ID, ElementPath.APPOINTMENT_FACILITY_ID))

            selector.select_by_index(facility_id)
            datepicker = WebDriverWait(browser, Constants.LOGIN_WAIT_TIMEOUT_SEC).until(ec.presence_of_element_located((By.ID, ElementPath.DATE_PICKER_ID))).click()

            appointments = browser.find_elements(By.CSS_SELECTOR, ElementPath.ACTIVE_DAY_CELL_SELECTOR)
            for _ in range(1, 6):
                log.debug(appointments)
                browser.find_element(By.XPATH, ElementPath.NEXT_MONTH_XPATH).click()
                appointments = browser.find_elements(By.CSS_SELECTOR, ElementPath.ACTIVE_DAY_CELL_SELECTOR)
                if len(appointments) > 0:
                    earliest_appointment = appointments[0]
                    day = int(earliest_appointment.find_elements(By.CSS_SELECTOR, '*')[0].text)
                    month = int(earliest_appointment.get_attribute(ElementPath.DAY_CELL_MONTH_ATTRIBUTE)) + 1
                    year = int(earliest_appointment.get_attribute(ElementPath.DAY_CELL_YEAR_ATTRIBUTE))
                    new_appointment_date = "{0}-{1}-{2}".format(year, month, day)
                    new_app_datetime = datetime(year, month, day)
                    log.info("found appointments! Earliest date: {0}".format(new_appointment_date))
                    log.info("current appointment: {0}".format(current_app_datetime))
                    log.info("new appointment: {0}".format(new_app_datetime))

                    if new_app_datetime < current_app_datetime:
                        scheduler.schedule_app(earliest_appointment, new_appointment_date, telegram_chat_id)
                        current_app_datetime = new_appointment_date
                    else:
                        scheduler.dont_schedule_app(telegram_chat_id)
                else:
                    log.info("no appointments! Moving to next month...")
    except Exception as e:
        log.error(e)
        browser.save_screenshot(Constants.EXCEPTION_SCREENSHOT_PATH)
    finally:
        browser.quit()
        log.info("done")


if __name__ == "__main__":
    #find_appointment(Constants.MACOS_CONTEXT)

    #find_appointment(Constants.DEFAULT_CONTEXT)
    #todo: use tenacity
    try:
        for i in range(1, 100):
            find_appointment(Constants.DEFAULT_CONTEXT)
            import time

            time.sleep(60*10)
    except Exception as e:
        log.error(e)

